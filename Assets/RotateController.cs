﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RotateController : MonoBehaviour {

    public GameObject rotate;
    public Text fpsTxt;
    public Text updTypeTxt;

    void Start()
    {
        updTypeTxt.text = rotate.GetComponent<Rotate>().rotateType.ToString();
        fpsTxt.text = rotate.GetComponent<Rotate>().fps.ToString();
    }

    public void SetTypeUpdate()
    {
        rotate.GetComponent<Rotate>().NextUpdateType();
        updTypeTxt.text = rotate.GetComponent<Rotate>().rotateType.ToString();
    }

    public void SetFps()
    {
        rotate.GetComponent<Rotate>().NextFps();
        fpsTxt.text = rotate.GetComponent<Rotate>().fps.ToString();
    }
}
