﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    public enum RotateT { FixedUpdate, Update, LateUpdate };
    public RotateT rotateType;
    public Vector3 rotationSpeedVector;

    public float timeMax = 0.01658f;
    public float speedScale = 0.6f;
    public int fps;
    Vector3 r;

    private int enumCount;

    void Start()
    {
        enumCount = Enum.GetNames(typeof(RotateT)).Length;
        Application.targetFrameRate = fps;
    }

    public void NextUpdateType()
    {
        int num = (int)rotateType;
        if (num >= enumCount-1)
        {
            num = 0;
        }
        else
        {
            num++;
        }

        rotateType = (RotateT)num;
    }

    public void NextFps()
    {
        fps += 5;
        if(fps > 60){
            fps = 30;
        }
        Application.targetFrameRate = fps;
    }

    void LateUpdate()
    {
        if (rotateType == RotateT.LateUpdate)
        {
            rot(rotationSpeedVector);
        }
    }

    void Update()
    {
        if (Time.deltaTime >= timeMax)
        {
            Debug.Log(Time.deltaTime);
        }
        if (rotateType == RotateT.Update)
        {
            rot(rotationSpeedVector);
        }
    }

    void FixedUpdate()
    {
        if (rotateType == RotateT.FixedUpdate)
        {
            rot(rotationSpeedVector);
        }
    }

    void rot(Vector3 rotationSpeedVecto)
    {
        transform.Rotate(Vector3.forward, rotationSpeedVector.z * Time.deltaTime);
    }
}